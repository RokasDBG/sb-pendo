module.exports = function() {
    var gulp = require('gulp');
    var htmlrender = require('gulp-htmlrender');
    var htmlbeautify = require('gulp-html-beautify');
    var removeHtmlComments = require('gulp-remove-html-comments');
    var concat = require('gulp-concat');
    var less = require('gulp-less');
    var rename = require("gulp-rename");
    var config = require('./config.js')();

    var jsTask = gulp.src(config.guideCenter.js.src)
                    .pipe(concat(config.guideCenter.js.file))
                    .pipe(gulp.dest(config.guideCenter.js.dist));

    var lessTask = gulp.src(config.guideCenter.less.src)
                    .pipe(less())
                    .pipe(rename(config.guideCenter.less.file))
                    .pipe(gulp.dest(config.guideCenter.less.dist));

    var htmlTask = gulp.src(config.guideCenter.html.src)
                    .pipe(removeHtmlComments())
                    .pipe(htmlrender.cache())
                    .pipe(htmlrender.render())
                    .pipe(htmlbeautify())
                    .pipe(gulp.dest(config.guideCenter.html.dist));

    return jsTask, lessTask, htmlTask
};