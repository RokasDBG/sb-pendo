module.exports = function () {
    const gc = './guide-center';

    const config = {
        guideCenter: {
            js: {
                src: `${gc}/src/scripts/*.js`,
                file: 'pendo.js',
                dist: `${gc}/dist/`
            },
            less: {
                src: `${gc}/src/styles/styles.less`,
                file: 'pendo.css',
                dist: `${gc}/dist/`
            },
            html: {
                src: `${gc}/src/pendo.html`,
                dist: `${gc}/dist/`
            }
        }
    };

    return config;
}
