(function wireGuideAdvanceButton(step) {
    step && step.attachEvent(step.guideElement[0], 'click', function (e) {
        var advanceButton = pendo.dom(e.target || e.srcElement).closest('._pendo-guide-next_');
        var previousButton = pendo.dom(e.target || e.srcElement).closest('._pendo-guide-back_');
        var dissmissButton = pendo.dom(e.target || e.srcElement).closest('._pendo-guide-dismiss_');
        var onboardButton = pendo.dom(e.target || e.srcElement).closest('._pendo-guide-show-onboarding_');
        if (advanceButton.length) {
            pendo.onGuideAdvanced();
        } else if (previousButton.length) {
            pendo.onGuidePrevious();
        } else if (dissmissButton.length) {
            pendo.onGuideDismissed();
        } else if (onboardButton.length) {
            pendo.showLauncher();
            document.getElementById('pendo-onboarding').click();
            pendo.onGuideDismissed();
        }
    });
})(step,guide);