(function wireGuideAdvanceButton(step) {
    step && step.attachEvent(step.guideElement[0], 'click', function (e) {
        var advanceButton = pendo.dom(e.target || e.srcElement).closest('._pendo-guide-next_');
        var previousButton = pendo.dom(e.target || e.srcElement).closest('._pendo-guide-back_');
        if (advanceButton.length) {
            pendo.onGuideAdvanced();
        } else if (previousButton.length) {
            pendo.onGuidePrevious();
        }
    });
})(step,guide);