/* global step */
(function (step) {
    step && step.attachEvent(step.guideElement[0], 'change', function () {
        var template = step.attributes && step.attributes.variables || {};
        var poll = step.guideElement.find('._pendo-poll_');
        var radio = step.guideElement.find('[name="pendo-poll-choice"]:checked');
        if (!radio.length || template.followUpType !== 'score') return;
        var choice = parseInt(radio[0].value, 10);
        poll.removeClass('_pendo-poll-detractor_ _pendo-poll-neutral_ _pendo-poll-promoter_');
        var npsResponseClass = choice < 7 ? 'detractor' : choice > 8 ? 'promoter' : 'neutral';
        poll.addClass('_pendo-poll-' + npsResponseClass + '_');
    });
})(step);